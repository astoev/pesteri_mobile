# README #

Limited version of PESTERI (http://iskar-speleo.org/drupal/?q=PESTERI) on Android devices.

Note, Android 9+ might not properly work due Android storage model update and PHP Server app deprecation.

### Device configuration ###

* Copy the modified version of PESTERI (see below) to the device.
* Install [Server for PHP](https://play.google.com/store/apps/details?id=com.esminis.server.php) and grant file permissions. Will probably work with other apps also.
* It will ask to install PHP runtime, select any 5.6.x (have not tested newer)
* Set data directory to point to the PESTERI folder and port to 8888
* Start the server. Sometimes it is better to disable power saving mode.
* Open [localhost](http://localhost:8888)

### PESTERI preparation ###

* Copy the contents of PESTERI/htdocs to the root of your devices' storage under folder www/public (most of it not used yet)
* Use the contents of this repo to replace the www/public/cgi-bin and www/config folders
* Convert the latest `pesteri` database file from sqlite2 to sqlite3 format:
``` 
sqlite pesteri .dump | iconv -f windows-1251 -t utf-8| sqlite3 pesteri_2018.DB
```. Optionally remove EKKATE records where no cave info is attached TODO
* Use it to replace www/public/cgi-bin/pesteri

### TODOs ###

1. Currently works only search by city
2. Link to view the cave info need to be fixed