<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">

<?php
/* (c) Ivo Tachev, ivotachev@mail.bg
License: The PHP License, http://www.php.net/license/3_0.txt
*/

include 'sqlite3.php';
$tt_database="pesteri";
$DB_version="1.2"; // required database schema version

// First, check how we are called:
$prm = $_POST;

// ������ (������ 1.0) ���������� �������� �� �������� ���, �� �� �� �� ������� ����������� �� ������.
// define regional codes list:
$region=array(101=>"������-�������������",102=>"���������-����������",103=>"�������� - ��������",104=>"������������-��������",105=>"�������������������",106=>"���������-�������",107=>"��������",108=>"�����������",201=>"�������������",202=>"�������",203=>"���������",204=>"��������",205=>"���������",206=>"������������-�����������",207=>"�������",208=>"���������-����������",209=>"�����������",210=>"�����������-������������",211=>"�������",212=>"���������",213=>"����������-���������",214=>"����������-�����������",215=>"���������-���������",216=>"���������",217=>"���������",218=>"����������",219=>"������������-���������",301=>"���������",302=>"��������-��������-��������",303=>"�����������-��������",304=>"�������",305=>"��������",306=>"�������-���������",307=>"����� �������",308=>"��������",309=>"����������",310=>"������������",401=>"���������",402=>"���������-���������",403=>"��������",404=>"��������",405=>"������������",406=>"��������",407=>"������������",408=>"����������",409=>"����������",410=>"���������",411=>"��������",412=>"���������",413=>"�������������");

// If no valid parameters given then display start menu:
if (!($prm["by_EKNM"]) & !($prm['by_EKNM']=='0') & !($prm['by_name']) & !($prm['ID']) & !($prm['spravka']) & !($prm['by_region']) & !($prm['by_district'])) {

$db_id = sqlite_open($tt_database,0666);

// Check database version:
$sql="select * from VERSION;";

$DBver = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
if ( $DB_version <> $DBver[0] ) { echo "<div align=center><font color=red><b>������ ������ �� ����������� �� ������ �����!</b></font></div>"; }

	//some HTML code
	echo '<title>������ - ������</title></head><body marginwidth="0" bgcolor="#dffff9">
	<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div><div align="center"><hr></div>
	<div align="center"><h3><b><font color="blue">������� �� ������</font></b></h3></div><br>';

	echo "<form action='PESTERI.php' method='post' enctype='application/x-www-form-urlencoded'>
	<div align='center'><font size='+1'>�� �������: </font>
	<select name='by_EKNM'>
	<option value=''>���� ������� �������</option>
	<option value='00000'>? ���������� ?</option>";

	// now we get all location names from the database to form a selection list
	$sql = 'SELECT EK_ATTE.EKATTE,EK_ATTE.T_V_M,EK_ATTE.NAME,EK_OBL.NAME FROM EK_ATTE, EK_OBL WHERE EK_OBL.OBLAST = EK_ATTE.OBLAST ORDER BY EK_ATTE.NAME';
	$qrez = sqlite_unbuffered_query($db_id,$sql);
	while ($q_row = sqlite_fetch_array($qrez)) {
		echo "<option value=$q_row[0]>$q_row[1] $q_row[2], ���. $q_row[3]</option>";
	}
	echo "</select>&nbsp;&nbsp;&nbsp;";

	sqlite_close ($db_id);

	// here we have our location name selection list ready
	// and proceed with the HTML code

	echo '<font color="red" size="+1"><i>(�/���)</i></font>&nbsp;&nbsp;&nbsp;<font size="+1">�� (���� ��) ���: </font><input name="by_name" type="text"><font color=red>(�������� �������: &quot;*&quot; � &quot;?&quot;)</font></div><br><div align="center"><input type=checkbox name=with_IZBOD>������ ���� � ���. ����� ���� ������</div>&nbsp;&nbsp;
	<div align="center"><font size="+1">�������� �� ���. �����:&nbsp;</font><input name="ID" size="5" maxlength="5" type="text"><br><font color="red">(�� ��������� �� ���� ������ ������� ������������ �������� ����� � ���. �����.)<br>(�� ����� �� ��������� �� ������ �� ������ ����� ������� ������������ ������ �������� ����� � ��������������� �����.)</font></div>
	<br>

	<div align="center"><font size="+1">������� �� �����:&nbsp;</font><select name="by_region"><option value="">���� ������ �����</option>';

	 // generate region selection list:
	function reg_opts($reg, $idx) {
		echo "<option value=$idx>$reg ($idx)</option>";
	}
	array_walk($region, 'reg_opts');

	echo '</select>';

	echo '&nbsp;&nbsp;&nbsp;<font size="+1">������� �� ������:&nbsp;</font><select name="by_district"><option value="">���� ������� ������</option>';

	 // generate district selection list:
	$sql = 'SELECT * FROM EK_OBL;';
	$qrez = sqlite_unbuffered_query($db_id,$sql);
	while ($obl = sqlite_fetch_array($qrez)) {
		echo "<option value=$obl[0]>$obl[1]</option>";
	}

	echo '</select></div><br>;

	<div align="center"><input type=checkbox name=with_DUBL_ID>������ � ���������!&nbsp;&nbsp;<font size="+1"><input value="�����" align="baseline" type="submit"></font></div>
	</form>
	<br>
	<div align="center"><hr></div>
	<div align="center"><h3><strong><font color="blue">���� ������� �� ������ �����</font></strong></h3></div>
	<br>
	<form action=spravki.php method="POST" enctype="application/x-www-form-urlencoded">
	<div align="center"><select name="spravka">
	<option value="1">���������� - �� ����������</option>
	<option value="2">���������� - �� ���������� ������</option>
	<option value="3">���������� - �� ��������� �� ��������������</option>
	<option value="4">������ - �� ����������� (��� 100 �)</option>
	<option value="5">������ - �� ������� (��� 1000 �)</option>
	</select>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input name="search" value="������" align="baseline" type="submit"></div>
	</form>
	<br><br>
	<div align="center"><hr></div>
	<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004-2009 �.</div></font>
	</body>
	</html>';
	// End of start page.
}

// ##############################################################################
// D_ID already specified => display cave view/edit page.
// Include dataform.php here ?

// Check whether we have the ID given as a parameter and if so, proceed to the display/edit page.
if ($prm['ID']) {
	$prm['by_name'] = ''; // ignore this parameter
	$prm['spravka'] = ''; // ignore this parameter
	$prm['by_region'] = ''; // ignore this parameter
	$prm['by_district'] = ''; // ignore this parameter
	$ID=$prm['ID'];

	echo '<title>����� �� ������ - �������/�����������</title></head>
	<body marginwidth="0" bgcolor="#dffff9">
	<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
	<div align="center"><hr></div>';

	echo "<div align='center'>������� ���. ����� �� ������: $ID.<br>";

	$db_id = sqlite_open("pesteri",0666);
	$sql="SELECT D_ID FROM PEST WHERE D_ID=$ID;";
	$maxID = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));

	if (! $maxID[0]) {
		echo '���� ������ ������ � ������, �� �� �������� ����� �� ���� ������.<br>';
  		if ($prm['by_EKNM'] == '') {
			echo '<font size="+1" color="red">�� ��������� �� ���� ������ � ��������� ������. ����, ������� �� ������� � �������� �������� �����.</font><br>';
		}
		else { echo "<br><form action='dataform.php' method='POST' enctype='application/x-www-form-urlencoded'><input type='hidden' name='ID' value=$ID><input type='hidden' name='EKATTE' value=" + $prm['by_EKNM'] + "><input value='�����������' align='baseline' type='submit'></form><br></div>"; }
	}
	else {
		$sql="SELECT EKATTE FROM PEST WHERE D_ID=$ID;";
		$old_EKATTE=sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
		if ($prm['by_EKNM'] > 0 && $prm['by_EKNM'] <> $old_EKATTE[0]) {
			echo "<div align='middle'><font color='red' size='+1'>������� ��� �������� �����, ����� � �������� �� ���������� � ������ ������� �� ��������� ������!<br>	����, ������� �� ������� � ����������� ������.<br>	��� �������� ������ �� ������������� ��������� �� ��������, ��������� &quot;�����������&quot;<br><br>
			<form action='dataform.php' method='POST' enctype='application/x-www-form-urlencoded'><input type='hidden' name='ID' value=$ID><input type='hidden' name='EKATTE' value=" + $prm['by_EKNM'] + "><input value='�����������' align='baseline' type='submit'>
			</form></font></div>";
		}
		else {
			echo '���������� ��� ���������� �� �����������...<br>';
			echo "<br><form action='dataform.php' method='POST' enctype='application/x-www-form-urlencoded'><input type='hidden' name='ID' value=$ID><input value='�����������' align='baseline' type='submit'></form><br></div>";
		}
	}

	sqlite_close ($db_id);

	// proceed with the HTML code
	echo '<br><br><br>
	<div align="center"><hr></div>
	<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004-2009 �.</div></font>
	</body>
	</html>';
	// End of cave selection page.
}

// End of cave view/edit module.
// ##############################################################################

// Perform search based on either by_EKNM or by_name, or both.
if (($prm['by_EKNM'] || $prm['by_EKNM']=='0' || $prm['by_name']) && $prm['ID'] == '' ) {
	$prm['spravka'] = ''; // ignore this parameter
	$prm['by_region'] = ''; // ignore this parameter
	$prm['by_district'] = ''; // ignore this parameter
	//some HTML code
	echo '<title>������ - ������</title></head>
	<body marginwidth="0" bgcolor="#dffff9">
	<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
	<div align="center"><hr></div>';

	// now we get all location names from the database to form a selection list
	$db_id = sqlite_open('pesteri',0666);

	// display the name of location in the page title, if given
	if ($prm['by_EKNM'] || $prm['by_EKNM']=='0') {
		$sql = "SELECT EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBL.NAME, EK_OBST.NAME FROM EK_ATTE, EK_OBL, EK_OBST WHERE EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_ATTE.EKATTE = $prm[by_EKNM] AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA";
		$qrez = sqlite_unbuffered_query($db_id,$sql);
		while ($q_row = sqlite_fetch_array($qrez)) {
			echo "<div align=center><h3><b><font color=blue>������ � ��������� �� $q_row[0] $q_row[1], ���. $q_row[3], ���. $q_row[2]";
			if ($prm['with_IZBOD']) { echo " � ������ � ������� ����� ������ ������"; }
			echo "</font></b></h3></div><br>";
		}

		// display the number of caves for the given EKATTE
		$sql="SELECT COUNT(D_ID) FROM PEST WHERE EKATTE = '$prm[by_EKNM]'";
		$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"$sql;"));
		echo "<div align=left>���� ������ � ���������: $cnt[0].</div>";
		if ($prm['with_IZBOD']) {
				$sq_name = "SELECT NAME FROM EK_ATTE WHERE EKATTE=" + $prm['by_EKNM'] + ";";
				$nam = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sq_name));
				$sql = "$sql OR (EKATTE<>" + $prm['by_EKNM'] + " AND IZBOD LIKE '%$nam[0]%')";
			$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"$sql;"));
		}

		if ($cnt[0] == '0') {echo "<div align=center>(���� �����)</div>";}

		else {
			echo "<form action='dataform_RO.php' method='POST' enctype='application/x-www-form-urlencoded'>
			<div align='center'><table rules='nowrap' border='1' align='left'>
			<tr align='center' valign='center' bgcolor='yellow'><td width='10' align='center' valign='center'><font size='+3' color='red'>*</font></td><td width='40' align='center' valign='center'>���. �</td><td width='60' align='center' valign='center'>���</td><td width='280' align='center' valign='center'>���</td><td width='200' align='center' valign='center'>��������</td><td width='40' align='center' valign='center'>����.</td><td width='30' align='center' valign='center'>- �</td><td width='30' align='center' valign='center'>+ �</td><td width='200' align='center' valign='center'>��������</td><td width='160' align='center' valign='center'>���. �����</td><td width='20' align='center' valign='center'>���.</td><td width='40' align='center' valign='center'>�����</td><td width='50' align='center' valign='center'>�����. N;E</td></tr>";

			// generate the list of caves found by EKNM

			if ($prm['with_DUBL_ID']) { $sql = "SELECT PEST.D_ID, PEST.CODE, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, PEST.OPIS, PEST.IZBOD, PEST.SWNO, PEST.METRA, GIS.X, GIS.Y FROM PEST, GIS WHERE GIS.D_ID = PEST.D_ID "; }
			else { $sql = "SELECT PEST.D_ID, PEST.CODE, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, PEST.OPIS, PEST.IZBOD, PEST.SWNO, PEST.METRA, GIS.X, GIS.Y FROM PEST, GIS WHERE GIS.D_ID = PEST.D_ID AND PEST.DUBL_ID = '' "; }


			// both parameters could be present, so
			if ($prm['by_name']) {
				if ($prm['with_IZBOD']) {
				$sq_name = "SELECT NAME FROM EK_ATTE WHERE EKATTE=" + $prm['by_EKNM'] + ";";
				$nam = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sq_name));
				$sql = "$sql AND (PEST.EKATTE = " + $prm['by_EKNM'] + " OR (PEST.EKATTE<>" + $prm['by_EKNM'] + " AND PEST.IZBOD GLOB '*$nam[0]*')) AND (PEST.NAME GLOB '*$prm[by_name]*' OR PEST.DUBLNAME GLOB '*$prm[by_name]*') ORDER BY PEST.D_ID;";
				}
				else
				{ $sql = "$sql  AND PEST.EKATTE = " + $prm['by_EKNM'] + " AND (PEST.NAME GLOB '*$prm[by_name]*' OR PEST.DUBLNAME GLOB '*$prm[by_name]*') ORDER BY PEST.D_ID;"; }
			}
			else {
				if ($prm['with_IZBOD']) {
				$sq_name = "SELECT NAME FROM EK_ATTE WHERE EKATTE=$prm[by_EKNM];";
				$nam = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sq_name));
				$sql = "$sql AND (PEST.EKATTE = $prm[by_EKNM] OR (PEST.EKATTE<>$prm[by_EKNM] AND PEST.IZBOD GLOB '*$nam[0]*')) ORDER BY PEST.D_ID;";
				}
				else
				{ $sql = "$sql  AND PEST.EKATTE = $prm[by_EKNM] ORDER BY PEST.D_ID;"; }
			}

			$qrez = sqlite_unbuffered_query($db_id,$sql);
			while ($q_row = sqlite_fetch_array($qrez)) {
				echo "<tr align=left valign=center><td align=center valign=center bgcolor=red><input type=radio name=ID value=$q_row[0] align=middle></td><td align=right valign=center>$q_row[0]</td><td align=center valign=center>$q_row[1]</td><td align=left valign=center>$q_row[2]</td><td align=left valign=center>$q_row[3]</td><td align=right valign=center>$q_row[4]</td><td align=right valign=center>$q_row[5]</td><td align=right valign=center>$q_row[6]</td><td align=left valign=center>$q_row[7]</td><td align=left valign=center>$q_row[8]</td><td align=left valign=center>$q_row[9]</td><td align=right valign=center>$q_row[10]</td><td align=left valign=center>$q_row[12]; $q_row[11]</td></tr>";
			}
			echo '</table></div><br><br><br><div  align=left><input type="submit" value="�������" align="left"></div></form>';
		}
	}
	else {
		$nq=$prm['by_name'];
		// we have only a search parameter given
		echo "<div align='center'><h3><b><font color='blue'>������ �� ��������� �������� $nq<font></b></h3></div><br>";
		$sql = "SELECT COUNT(PEST.D_ID) FROM PEST, EK_ATTE, EK_OBST, EK_OBL WHERE (PEST.NAME GLOB '*$nq*' OR PEST.DUBLNAME GLOB '*$nq*') AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_ATTE.EKATTE = PEST.EKATTE AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA ORDER BY PEST.EKATTE;";
		$cnt = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
		if ($cnt[0] == '0') {echo "<div align=center>(���� ��������)</div>";}

		else {
			echo "<form action='$phpself' method='POST' enctype='application/x-www-form-urlencoded'>
			<div align='center'><table rules='nowrap' border='1' align='left'><tr align='center' valign='center' bgcolor='yellow'><td width='10' align='center' valign='center'><font size='+3' color='red'>*</font></td><td width='40' align='center' valign='center'>���. �</td><td width='60' align='center' valign='center'>���</td><td width='280' align='center' valign='center'>���</td><td width='200' align='center' valign='center'>��������</td><td width='40' align='center' valign='center'>����.</td><td width='30' align='center' valign='center'>- �</td><td width='30' align='center' valign='center'>+ �</td><td width='200' align='center' valign='center'>�������</td><td width='160' align='center' valign='center'>������</td><td width='60' align='center' valign='center'>������</td></tr>";

			// generate the search query
			
			$sql = "SELECT PEST.D_ID, PEST.CODE, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME FROM PEST, EK_ATTE, EK_OBST, EK_OBL WHERE (PEST.NAME GLOB '*$nq*' OR PEST.DUBLNAME GLOB '*$nq*') AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_ATTE.EKATTE = PEST.EKATTE AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA ORDER BY PEST.EKATTE;";
			// now generate the list of caves according to the specified criteria
			$qrez = sqlite_unbuffered_query($db_id,$sql);
			while ($q_row = sqlite_fetch_array($qrez)) {
				echo "<tr align=left valign=center><td align=center valign=center bgcolor=red><input type=radio name=ID value=$q_row[0] align=middle></td><td align=right valign=center>$q_row[0]</td><td align=center valign=center>$q_row[1]</td><td align=left valign=center>$q_row[2]</td><td align=left valign=center>$q_row[3]</td><td align=right valign=center>$q_row[4]</td><td align=right valign=center>$q_row[5]</td><td align=right valign=center>$q_row[6]</td><td align=left valign=center>$q_row[7] $q_row[8]</td><td align=left valign=center>$q_row[9]</td><td align=left valign=center>$q_row[10]</td></tr>";
			}
			echo '</table></div><br><br><br><div  align=left><input type="submit" value="�������/�������" align="left"></div></form>';
			}
	}

	sqlite_close ($db_id);

	// here we have our location name selection list ready
	// and proceed with the HTML code
	echo '<br><br><br>
	<div align="center"><hr></div>
	<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004-2009 �.</div></font>
	</body>
	</html>';
	// End of cave selection page - by EKATTE.
}

// Perform search based on a region:
if ($prm['by_region']) {
	$prm['spravka'] = ''; // ignore this parameter
	$prm['by_district'] = ''; // ignore this parameter

	//some HTML code
	echo '<title>������ - ������</title></head>
	<body marginwidth="0" bgcolor="#dffff9">
	<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
	<div align="center"><hr></div>';

	// display the name of the region in the page title
	$idx=$prm['by_region'];
	echo "<div align=center><h3><b><font color=blue>������ � " + $region[$idx] + " ����� (� " + $prm['by_region'] + "):</font></b></h3></div><br>";

	$db_id = sqlite_open("pesteri_2018.DB",0666);

	// display the number of caves in the selected region
	$kod = $prm['by_region'] * 1000;
	$kod_next = $kod + 1000;

	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE CODE>$kod AND CODE<$kod_next;"));
	echo "<div align=left>���� ������ � ������: $cnt[0].</div>";

	echo "<form action='dataform_RO.php' method='POST' enctype='application/x-www-form-urlencoded'>
	<div align='center'><table rules='nowrap' border='1' align='left'>
	<tr align='center' valign='center' bgcolor='yellow'><td width='10' align='center' valign='center'><font size='+3' color='red'>*</font></td><td width='60' align='center' valign='center'>���</td><td width='40' align='center' valign='center'>���. �</td><td width='280' align='center' valign='center'>���</td><td width='200' align='center' valign='center'>��������</td><td width='40' align='center' valign='center'>����.</td><td width='30' align='center' valign='center'>- �</td><td width='30' align='center' valign='center'>+ �</td><td width='200' align='center' valign='center'>�������</td><td width='160' align='center' valign='center'>������</td><td width='20' align='center' valign='center'>������</td><td width='40' align='center' valign='center'>��������</td><td width='50' align='center' valign='center'>�����. N;E</td></tr>";

	// generate the list of caves found by region
	if ($prm['with_DUBL_ID']) { $sql = "SELECT PEST.CODE, PEST.D_ID, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME, PEST.OPIS, GIS.X, GIS.Y FROM PEST, EK_ATTE, EK_OBST, EK_OBL, GIS WHERE GIS.D_ID = PEST.D_ID AND PEST.CODE>$kod AND PEST.CODE<$kod_next AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA AND EK_ATTE.EKATTE = PEST.EKATTE ORDER BY CODE;"; }
	else { $sql = "SELECT PEST.CODE, PEST.D_ID, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME, PEST.OPIS, GIS.X, GIS.Y FROM PEST, EK_ATTE, EK_OBST, EK_OBL, GIS WHERE GIS.D_ID = PEST.D_ID AND PEST.DUBL_ID = '' AND PEST.CODE>$kod AND PEST.CODE<$kod_next AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA AND EK_ATTE.EKATTE = PEST.EKATTE ORDER BY CODE;"; }

	$qrez = sqlite_unbuffered_query($db_id,$sql);
	while ($q_row = sqlite_fetch_array($qrez)) {
		echo "<tr align=left valign=center><td align=center valign=center bgcolor=red><input type=radio name=ID value=$q_row[1] align=middle></td><td align=center valign=center>$q_row[0]</td><td align=right valign=center>$q_row[1]</td><td align=left valign=center>$q_row[2]</td><td align=left valign=center>$q_row[3]</td><td align=right valign=center>$q_row[4]</td><td align=right valign=center>$q_row[5]</td><td align=right valign=center>$q_row[6]</td><td align=left valign=center>$q_row[7] $q_row[8]</td><td align=left valign=center>$q_row[9]</td><td align=left valign=center>$q_row[10]</td><td align=left valign=center>$q_row[11]</td><td align=left valign=center>$q_row[13]; $q_row[12]</td></tr>";
	}

	echo '</table></div><br><br><br>
	<div  align=left><input type="submit" value="�������/�������" align="left"></div>
	</form>';

	sqlite_close ($db_id);

	// here we have our selection list by region ready and proceed with the HTML code
	echo '<br><br><br>
	<div align="center"><hr></div>
	<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004-2009 �.</div></font>
	</body>
	</html>';
	// End of cave selection page - by region.
}

// Perform search based on a district:
if ($prm['by_district']) {
	$prm['spravka'] = ''; // ignore this parameter
	$prm['by_region'] = ''; // ignore this parameter

	//some HTML code
	echo '<title>������ - ������</title></head>
	<body marginwidth="0" bgcolor="#dffff9">
	<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
	<div align="center"><hr></div>';

	$db_id = sqlite_open('pesteri_2018.DB',0666);

	// display the number of caves in the selected district

	$sql = "SELECT NAME FROM EK_OBL WHERE OBLAST = '$prm[by_district]'";
		$obl = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));

	// display the name of the district in the page title
	echo "<div align=center><h3><b><font color=blue>������ � ������ $obl[0]:</font></b></h3></div><br>";

		// display the number of caves for the given district
		$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(PEST.D_ID) FROM PEST,EK_ATTE WHERE PEST.EKATTE = EK_ATTE.EKATTE AND EK_ATTE.OBLAST = '$prm[by_district]';"));
		echo "<div align=left>���� ������ � ��������: $cnt[0].</div>";

		if ($cnt[0] == '0') {echo "<div align=center>(���� �����)</div>";}

		else {

	echo "<form action='dataform_RO.php' method='POST' enctype='application/x-www-form-urlencoded'>
	<div align='center'><table rules='nowrap' border='1' align='left'>
	<tr align='center' valign='center' bgcolor='yellow'><td width='10' align='center' valign='center'><font size='+3' color='red'>*</font></td><td width='60' align='center' valign='center'>���</td><td width='40' align='center' valign='center'>���. �</td><td width='280' align='center' valign='center'>���</td><td width='200' align='center' valign='center'>��������</td><td width='40' align='center' valign='center'>����.</td><td width='30' align='center' valign='center'>- �</td><td width='30' align='center' valign='center'>+ �</td><td width='200' align='center' valign='center'>�������</td><td width='160' align='center' valign='center'>������</td><td width='40' align='center' valign='center'>��������</td><td width='50' align='center' valign='center'>�����. N;E</td></tr>";

	// generate the list of caves found by district
	if ($prm['with_DUBL_ID']) { $sql = "SELECT PEST.CODE, PEST.D_ID, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME, PEST.OPIS, GIS.X, GIS.Y FROM PEST, EK_ATTE, EK_OBST, EK_OBL, GIS WHERE GIS.D_ID = PEST.D_ID AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA AND EK_ATTE.EKATTE = PEST.EKATTE AND EK_ATTE.OBLAST = '$prm[by_district]' ORDER BY EK_ATTE.OBSTINA, PEST.EKATTE, PEST.D_ID;"; }
	else { $sql = "SELECT PEST.CODE, PEST.D_ID, PEST.NAME, PEST.DUBLNAME, PEST.LEN, PEST.BOT, PEST.TOP, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME, PEST.OPIS, GIS.X, GIS.Y FROM PEST, EK_ATTE, EK_OBST, EK_OBL, GIS WHERE GIS.D_ID = PEST.D_ID AND PEST.DUBL_ID = '' AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA AND EK_ATTE.EKATTE = PEST.EKATTE AND EK_ATTE.OBLAST = '$prm[by_district]' ORDER BY EK_ATTE.OBSTINA, PEST.EKATTE, PEST.D_ID;"; }

	$qrez = sqlite_unbuffered_query($db_id,$sql);
	while ($q_row = sqlite_fetch_array($qrez)) {
		echo "<tr align=left valign=center><td align=center valign=center bgcolor=red><input type=radio name=ID value=$q_row[1] align=middle></td><td align=center valign=center>$q_row[0]</td><td align=right valign=center>$q_row[1]</td><td align=left valign=center>$q_row[2]</td><td align=left valign=center>$q_row[3]</td><td align=right valign=center>$q_row[4]</td><td align=right valign=center>$q_row[5]</td><td align=right valign=center>$q_row[6]</td><td align=left valign=center>$q_row[7] $q_row[8]</td><td align=left valign=center>$q_row[9]</td><td align=left valign=center>$q_row[11]</td><td align=left valign=center>$q_row[13]; $q_row[12]</td></tr>";
	}

	echo '</table></div><br><br><br>
	<div  align=left><input type="submit" value="�������" align="left"></div>
	</form>';

	sqlite_close ($db_id);

	// here we have our selection list by district ready and proceed with the HTML code
	echo '<br><br><br>
	<div align="center"><hr></div>
	<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004-2009 �.</div></font>
	</body>
	</html>';
	// End of cave selection page - by region.
}
}
?>
