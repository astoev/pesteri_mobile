<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>PESTERI - ������������ �� ������</title>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body bgcolor="#DFFFF9">
<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
<div align="center"><hr></div>
<div align="center"><h2>������������<b></b></h2></div><br>

<?php
/* (C)Ivo Tachev, ivotachev@mail.bg
License: The PHP License, http://www.php.net/license/3_0.txt
*/

include("templateze.php");

//---------some functions--------------------
//Needed to cope with messing single and double quotes. Single quotes get replaced by double quotes.

function array_html_encode(&$input) {
   $output = array();
   foreach ($input AS $key => $value) {
	if (is_array($input[$key])) {
	     $output[$key] = array_html_encode($value);
	   }
	else {
		$output[$key] = htmlspecialchars($value);
	}
   }
  return $output;
}

function array_html_decode(&$input) {
   $output = array();
   foreach ($input AS $key => $value) {
	if (is_array($input[$key])) {
	     $output[$key] = array_html_decode($value);
	   }
	else {
		$output[$key] = str_replace("'",'"', stripcslashes(htmlspecialchars_decode($value)));
	}
   }
  return $output;
}

//---------end of functions--------------------


// Some initialization, just to be sure
$ID='';
include 'sqlite3.php';
$tt_database="pesteri";
$DB_version="1.2"; // required database schema version
$phpself="biblio.php";

// Check database version:
$db_id = sqlite_open($tt_database,0666);
$sql="select * from VERSION;";
$DBver = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
if ( $DB_version <> $DBver[0] ) { echo "<div align=center><font color=red><b>������ ������ �� ����������� �� ������ �����!</b></font></div>"; }

// Fetch GET/POST vars
$prm = array_html_decode($_GET);
$prmp = array_html_decode($_POST);
$ID = $prm['ID']; // we shall need this in all cases

if ( ! $ID > 0 ) { // Simple check, ID should not be empty
	echo '<div style="text-align: center;"><b><font color="red">���������� ��������� �� �������!</font></b></div>';
}

// Get curent date to apply to TS timestamps:
$TS=date('Y-m-d',time());


// See how we are called:

if ( $prm['EXPORT'] ) { // Export updated records file
/*	$sql="select * from BIBLIO where TS > $DBVer[1];";
	$rez=sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
	*/
	echo "<div align=center><font color=red><b>��������� ��� �� � �����������.</b></font></div>";
	sqlite_close($db_id);
	echo '<br><div style="text-align: center;"><INPUT type="button" name="close" value="������� ���������" onclick="window.close()"></div><hr><font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004,2009 �.</div></font></body></html>'; // footer
	exit(0);
}

if ( $prm['biblio2ID'] <> '' && $ID <> '' &&  $prm['biblio2ID'] <> 'ADD'  ) { // Adding record BIBLIO.M3 to cave ID in ID2BIBLIO
	// First, check uniqueness of submitted ID<->biblio pair:
	$sql="select count (M3) from ID2BIBLIO where D_ID=$ID and M3='" + $prm['M3'] + "' and M3 not like 'd_%';";
	$rez=sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
	if ($rez[0] > 1) { echo "<p align='center'><font color='red'>������� ���� � ������� �� ���� ������!</font></p>"; }
	else { 
	$sql="insert into ID2BIBLIO (D_ID,M3,BIBNOTE,TS) values('$ID','$prm[biblio2ID]','$prm[BIBNOTE]','$TS');";
	$rez=sqlite_unbuffered_query($db_id,$sql); }
}
if ( $prm['remove'] && $ID && ( ! $prm['edit'] ) ) { // Deleting record M3 from cave ID in ID2BIBLIO
	$sql="select TS from ID2BIBLIO where M3='" + $prm['remove'] + "' and D_ID='$ID';";
	$oldTS=sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));

	if ( $oldTS[0] == $TS ) { // Record added same day, delete it
		$sql="delete from ID2BIBLIO where M3='$prm[remove]' and D_ID='$ID';"; }
	else { $sql="update ID2BIBLIO set M3='d_$prm[remove]',BIBNOTE='$oldTS[0]' TS='$TS' where M3='$prm[remove]' and D_ID='$ID';"; } // Leave reference to M3 and old timestamp

	$rez=sqlite_unbuffered_query($db_id,$sql);
}

if ( $prm[M3] && $ID &&  $prm[bib_note] && $prm[BIBNOTE] ) { // Update note BIBNOTE for record M3 to cave ID in ID2BIBLIO
	$sql="update ID2BIBLIO set BIBNOTE='$prm[BIBNOTE]', TS='$TS' where M3='$prm[M3]' and D_ID='$ID';";
	$rez=sqlite_unbuffered_query($db_id,$sql);
}

if ( ( $prm[edit] <> '' || $prm[biblio2ID] == 'ADD' ) && $ID <> '' ) { // Edit record M3 in BIBLIO
	$tple= new templateze("./biblio_edit-tpl.html");
	$tple->set("PHPSELF|$phpself");
	$tple->set("D_ID|$ID");
	if ( $prm[biblio2ID] == 'ADD' ) { // We are adding a new record in BIBLIO
		$sql="select max(M3) from BIBLIO;";
		$rez = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
		$prm[edit] = $rez[0] + 1;
	}
	// Edit an existing record in BIBLIO
	$sql="select * from BIBLIO where M3=$prm[edit];";
	$newdat = sqlite_fetch_all(sqlite_unbuffered_query($sql,$db_id),SQLITE_ASSOC);
	$newdata = array_html_encode($newdat);
	$rez=count($newdata);
	if ( $rez == 0 ) { $newdata = array( "0" => array( "M3" => $prm[edit], "TY" => "", "T1" => "", "T2" => "", "A1" => "", "A2" => "", "Y1" => "", "SP" => "", "EP" => "", "JF" => "", "VL" => "", "IS" => "", "CY" => "", "PB" => "", "KW" => "", "N2" => "", "SN" => "", "UR" => "", "L1" => "", "L2" => "" )); }
	$tple->set("M3|$prm[edit]");
	$tple->setloop("newdata");
	echo $tple->display();
	sqlite_close($db_id);
	echo '<hr><font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004,2009 �.</div></font></body></html>'; // footer
	exit(0);
}

if ( $prm['T1'] <> '' && $ID <> '' ) { // Apply data to BIBLIO in database
	$sql="select * from BIBLIO where M3=$prm[M3];";
	$rez=sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql)); //check if record is a new one
	if ( $rez[T1] <> '' ) { $sql = "update BIBLIO set TY='$prm[TY]',T1='$prm[T1]',T2='$prm[T2]',A1='$prm[A1]',A2='$prm[A2]',Y1='$prm[Y1]',SP='$prm[SP]',EP='$prm[EP]',JF='$prm[JF]',VL='$prm[VL]','IS'='$prm[IS]',CY='$prm[CY]',PB='$prm[PB]',KW='$prm[KW]',N2='$prm[N2]',SN='$prm[SN]',UR='$prm[UR]',L1='$prm[L1]',L2='$prm[L2]',TS='$TS' where M3='$prm[M3]';"; } // update an existing record
	else { $sql = "insert into BIBLIO values('$prm[M3]','$prm[TY]','$prm[T1]','$prm[T2]','$prm[A1]','$prm[A2]','$prm[Y1]','$prm[SP]','$prm[EP]','$prm[JF]','$prm[VL]','$prm[IS]','$prm[CY]','$prm[PB]','$prm[KW]','$prm[N2]','$prm[SN]','$prm[UR]','$prm[L1]','$prm[L2]','$TS');";	} // insert a new record

	$rez=sqlite_unbuffered_query($db_id,$sql);
	if ( $rez <> '' ) { echo "<div style='text-align: center;'><font color='red'><i>�������� � ��������������� ����� � ����� $prm[M3]: $prm[A1] &quot;$prm[T1]&quot;.</i></font></div><br>"; }
}

if ( $prm[SEARCH] <> '' && $ID <> '' ) { //We have to filter selection list entries by search criteria
	if ( $prm[FIELD1] == 'A1' || $prm[FIELD1] == 'T1' ) { $aux_field = str_replace('1','2',$prm[FIELD1]); $FIELD1 = "( $prm[FIELD1] glob '*$prm[CRIT1]*' or $aux_field glob '*$prm[CRIT1]*' )"; }
	else $FIELD1 = "$prm[FIELD1] glob '*$prm[CRIT1]*'";
	if ( $prm[FIELD2] == 'A1' || $prm[FIELD2] == 'T1' ) { $aux_field = str_replace('1','2',$prm[FIELD2]); $FIELD2 = "( $prm[FIELD2] glob '*$prm[CRIT2]*' or $aux_field glob '*$prm[CRIT2]*' )"; }
	else $FIELD2 = "$prm[FIELD2] glob '*$prm[CRIT2]*'";
	$sql_search = "and ( $FIELD1 $prm[OP1] $FIELD2 )";
//	echo $sql_search; //debug
}

// Only reading main form below:

//		$sql = "SELECT PEST.NAME,BIBLIO.M3,BIBLIO.A1,BIBLIO.T1,BIBLIO.T2,BIBLIO.JF,BIBLIO.IS,BIBLIO.VL,BIBLIO.Y1,BIBLIO.SP,BIBLIO.EP,BIBLIO.PB,BIBLIO.CY FROM PEST,ID2BIBLIO,BIBLIO WHERE PEST.D_ID = $ID and PEST.D_ID=ID2BIBLIO.D_ID and ID2BIBLIO.M3=BIBLIO.M3;";
		$sql = "SELECT NAME FROM PEST WHERE D_ID='$ID';";
		$cavename = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
		$biblio = sqlite_fetch_array(sqlite_unbuffered_query($db_id, "select count(D_ID) from ID2BIBLIO where D_ID='$ID';"));
		if ( ! ($biblio[0] > 0) ) { $nodata = '(���� ������������ �� ��������)'; }
		else {
			$sql = "SELECT BIBLIO.*,ID2BIBLIO.* FROM PEST,ID2BIBLIO,BIBLIO WHERE PEST.D_ID=$ID and PEST.D_ID=ID2BIBLIO.D_ID and ID2BIBLIO.M3=BIBLIO.M3 order by A1,A2,T1;";
			$alldata = sqlite_fetch_all(sqlite_unbuffered_query($sql,$db_id),SQLITE_ASSOC);
		}


$tpl = new templateze("./biblio_2cave-tpl.html");

$tpl->set("PHPSELF|$phpself");
$tpl->set("D_ID|$ID");
$tpl->set("NAME|$cavename[0]");
$tpl->set("NODATA|$nodata");

$tpl->setloop("alldata");

		$rez=sqlite_unbuffered_query("select count(M3) from BIBLIO;",$db_id);
		$sql = "SELECT PEST.NAME,BIBLIO.* FROM PEST,BIBLIO WHERE PEST.D_ID='$ID'" . " $sql_search" . " order by A1,A2,T1";
		if ( $rez[0] > 2000 ) { $sql=("$sql"." limit 2000"); $toomany_warn='� ������� �� �� �������� ������ ������. �������� ������� � ��������� �� �������.'; } //Limit selection list entries to 2000
		$all_data = sqlite_fetch_all(sqlite_unbuffered_query("$sql;",$db_id),SQLITE_ASSOC);
		function arr_trim($elem) { return(substr($elem,0,60)); } //Limit field length in selection list records to max. 60 chars
		foreach ($all_data as &$value) { $value = array_map("arr_trim", $value); }
//		unset($value);
//echo $sql;  //debug

$tpl->setloop("all_data");
$tpl->set("TOOMANY_WARN|$toomany_warn");
echo $tpl->display();


// End of dataform
// ######################################################

sqlite_close($db_id);

?>

<!--here we have our php code done and proceed with the end of HTML code-->
<hr>
<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004,2009 �.</div></font>
</body>
</html>
