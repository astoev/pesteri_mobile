<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>PESTERI - ����� �� ������</title>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body bgcolor="#DFFFF9">
<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
<div align="center"><hr></div>
<div align="center"><h2>����� �� ������<b></b></h2></div><br>

<?php
/* (c) Ivo Tachev, ivotachev@mail.bg, 2009
License: The PHP License, http://www.php.net/license/3_0.txt
*/

// Functions:

// Get HREF string of available text doc file (OD, GMO or TO type, TXT or PNG format)
function textdoc_href($textdoc_ID,$textdoc_type,$textdoc_type_bg) {
	$fnam = "../$textdoc_type/${textdoc_ID}_${textdoc_type}.txt";
	if ( file_exists($fnam) ) { $textdoc_href="<a href='$fnam'>��� $textdoc_type_bg</a>"; }
	else { $textdoc_href="<a href='../$textdoc_type/${textdoc_ID}_${textdoc_type}.png'>��� $textdoc_type_bg</a>"; }
	return $textdoc_href;
}


// End of Functions.

// Some initialization, just to be sure
$UPDATE='';
$EKATTE='';
$ID='';
include 'sqlite3.php';
$tt_database="pesteri";
$DB_version="1.2"; // required database schema version

// Define lists of option values as arrays
// When updating them do not forget to change the '<OPTION>' values in the dataform accordingly!
$EMOT = array ('�.�.', '������������ ������', '�������', '��������� ������', '��������� ������', '������ ������', '������� ������', '����');
$WATERT = array('�.�.', '����', '������', '���������� �����', '�����', '� ����� � ���� �����');
$Q_N_Y = array('�.�.', '��', '��');
$N_Y = array('F'=>'��', 'T'=>'��', '0'=>'��', '1'=>'��', ''=>'�.�.');
$JURT = array('�.�.', '�������� ������', '� ����. ����', '� ���. ����', '� ��������', '������� �����', '���. ��������', '�������������', '������������', '���� ������� ������');

// Fetch POST vars
$prm = $_POST;
$ID = $prm['ID']; // we shall need this in all cases
$EKATTE = $prm['EKATTE']; // we shall need this in all cases

$db_id = sqlite_open($tt_database,0666);

// Check database version:
$sql="select * from VERSION;";

$DBver = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
if ( $DB_version <> $DBver[0] ) { echo "<div align=center><font color=red><b>������ ������ �� ����������� �� ������ �����!</b></font></div>"; }

// construct sql if data has to be recorded/updated...
if (($prm['UPDATE'] == '1') & ($EKATTE <> '') & ($ID <> '')) {
	if ($prm['DUBL_ID'] <> '') {   // cannot update a duplicate cave!
	 	// we have to ensure all fields except D_ID, DUBL_ID and EKATTE are empty for duplicate caves:
		$sql = "REPLACE INTO PEST VALUES ('$ID','','','','$EKATTE','','','$prm[DUBL_ID]','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);
 		$sql = "REPLACE INTO GIS VALUES ('$ID','','','','','','','','','');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);
 		$sql = "REPLACE INTO MAP VALUES ('$ID','','','','','','','','','','','','','','','','','','','','','','','');";
 		$qrez = sqlite_unbuffered_query($db_id,$sql);
		$sql = "REPLACE INTO INFO VALUES ('$ID','','','','','','','','','','','','','','','','','','','','');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);

		$ID = $prm['DUBL_ID']; // we shall query the parent cave instead
		}
	else {
		//-----------------------------------------------------------------------------------
		// perform some form data validation:
		/* To do... */

		// end of form data validation.
		//-----------------------------------------------------------------------------------

		$sql = "REPLACE INTO PEST VALUES ('$ID','$prm[DATE]','$prm[AUTHOR]','$prm[CODE]','$prm[EKATTE]','$prm[NAME]','$prm[DUBLNAME]','$DUBL_ID','$prm[LEN]','$prm[BOT]','$prm[TOP]','$prm[ALT]','$prm[EBALT]','$prm[GUN]','$prm[PLUMB]','$prm[HOR]','$prm[GLAC]','$prm[REQ]','$prm[EQIP]','$prm[ROPE]','$prm[LAD]','$prm[TECH]','$prm[INFO]','$prm[NOINF]','$prm[SCINF]','$prm[OPIS]','$prm[WATER]','$prm[CHAN]','$prm[DIA]','$prm[BRA]','$prm[IZBOD]','$prm[SWNO]','$prm[CESTA]','$prm[SIF]','$prm[KROKI]','$prm[KLUB]','$prm[VOD]','$prm[EMO]','$prm[METRA]');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);

		// Check whether coordinates have been updated from cave data form. If so, enter today's date and operator's name:
		$sql="SELECT * FROM GIS WHERE D_ID = $ID;";
		$qrez = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));

		if ($prm['X'] == '' || $prm['Y'] == '') { // do not update empty values for coordinates
			// preserve values from database:
			$prm[X]=$qrez[1];
 			$prm[Y]=$qrez[2];
 			$prm[Z]=$qrez[3];
 			$prm[GPS_DATE]=$qrez[4];
 			$prm[GPS_WHO]=$qrez[5];
 			$prm[ZAB]=$qrez[6];
 			$prm[GPS_NOTES]=$qrez[7];
			$prm[CHECKED]=$qrez[9];
 			if ($prm[MARK] <> 'T') { $prm[MARK]=$qrez[8]; } // still, we can raise MARK flag
		}
		if ($qrez[1] <> $prm[X] || $qrez[2] <> $prm[Y]) { // yes, they are updated from the form
 			$prm[GPS_WHO]="��������-���";
 			$prm[GPS_DATE]=date("d.m.Y");
			$prm[CHECKED]='';
		}

		$sql = "REPLACE INTO GIS VALUES ('$ID','$prm[X]','$prm[Y]','$prm[Z]','$prm[GPS_DATE]','$prm[GPS_WHO]','$prm[ZAB]','$prm[GPS_NOTES]','$prm[MARK]','$qrez[9]');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);
		// End of coordinates update check.

		$sql = "REPLACE INTO MAP VALUES ('$ID','$prm[OD]','$prm[ODF]','$prm[GMO]','$prm[GMOF]','$prm[TO]','$prm[TOF]','$prm[MAP]','$prm[MAPC_TI]','$prm[MAPC_HP]','$prm[MAPC_RVP1]','$prm[MAPC_RVP2]','$prm[MAPC_NS]','$prm[MAPC_SR]','$prm[FT]','$prm[MAPL]','$prm[MAPF]','$prm[MAPGIF]','$prm[KROKIGIF]','$prm[CR]','$prm[FOTO]','$prm[FOTOF]','$prm[FISH_A]','$prm[FISH_B]');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);
		$sql = "REPLACE INTO INFO VALUES ('$ID','$prm[ROCK]','$prm[SEDI]','$prm[TECT]','$prm[MNT]','$prm[RELEF]','$prm[DYNA]','$prm[FORMS]','$prm[HYDROGEO]','$prm[T_XENI]','$prm[T_PHYLI]','$prm[T_BIO]','$prm[BIORED]','$prm[BIONEW]','$prm[BIO]','$prm[BIO_NOTES]','$prm[JUR]','$prm[NORMDOC]','$prm[STOP]','$prm[OTKRIL]','$prm[OSOB]');";
		$qrez = sqlite_unbuffered_query($db_id,$sql);
	}
 	$EKATTE=''; // reset this - query has to read EKATTE from the database
}

// We are just querying below
if ($EKATTE > 0) {
	if ($ID <> '') {  // We have just changed EKATTE, else this is a new entry with EKATTE selected
		$sql = "SELECT PEST.*,GIS.D_ID,GIS.X,GIS.Y,GIS.Z,GIS.DATE,GIS.WHO,GIS.ZAB,GIS.GPS_NOTES,GIS.MARK,MAP.*,INFO.*,GIS.CHECKED FROM PEST,GIS,MAP,INFO WHERE PEST.D_ID = $ID AND GIS.D_ID = $ID AND MAP.D_ID = $ID AND INFO.D_ID = $ID;";  // Note: TVM, NASMES, OBSTINA and OBLAST are the last four values in the resultset
		$alldata = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
	}
	$sql = "SELECT EK_ATTE.T_V_M,EK_ATTE.NAME,EK_OBST.NAME,EK_OBL.NAME FROM EK_ATTE,EK_OBST,EK_OBL WHERE EK_ATTE.EKATTE = $EKATTE AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA;";  // we shall display location by the EKATTE value passed
	$nm = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
	$alldata[93] = $nm[0];
	$alldata[94] = $nm[1];
	$alldata[95] = $nm[2];
	$alldata[96] = $nm[3];
	$alldata[97] = $EKATTE;
}

else {
	$sql = "SELECT PEST.*,GIS.D_ID,GIS.X,GIS.Y,GIS.Z,GIS.DATE,GIS.WHO,GIS.ZAB,GIS.GPS_NOTES,GIS.MARK,MAP.*,INFO.*,EK_ATTE.T_V_M,EK_ATTE.NAME,EK_OBST.NAME,EK_OBL.NAME,EK_ATTE.EKATTE,GIS.CHECKED FROM PEST,GIS,MAP,INFO,EK_ATTE,EK_OBST,EK_OBL WHERE PEST.D_ID = $ID AND GIS.D_ID = $ID AND MAP.D_ID = $ID AND INFO.D_ID = $ID AND EK_ATTE.EKATTE = ( SELECT EKATTE FROM PEST WHERE D_ID = $ID ) AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA;";  // Note: TVM, NASMES, OBSTINA and OBLAST are the last four values in the resultset
	$alldata = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
}

// unescape quotes in text values
$alldata[35] = stripslashes($alldata[35]);
$alldata[55] = stripslashes($alldata[55]);
$alldata[2] = stripslashes($alldata[2]);
$alldata[5] = stripslashes($alldata[5]);
$alldata[6] = stripslashes($alldata[6]);
$alldata[25] = stripslashes($alldata[25]);
$alldata[30] = stripslashes($alldata[30]);
$alldata[32] = stripslashes($alldata[32]);
$alldata[45] = stripslashes($alldata[45]);
$alldata[46] = stripslashes($alldata[46]); // GPS_NOTES
$alldata[92] = stripslashes($alldata[92]);
$alldata[75] = stripslashes($alldata[75]);
$alldata[76] = stripslashes($alldata[76]);
$alldata[77] = stripslashes($alldata[77]);
$alldata[78] = stripslashes($alldata[78]);
$alldata[79] = stripslashes($alldata[79]);
$alldata[80] = stripslashes($alldata[80]);
$alldata[86] = stripslashes($alldata[86]);
$alldata[87] = stripslashes($alldata[87]);
$alldata[89] = stripslashes($alldata[89]);
$alldata[90] = stripslashes($alldata[90]);
$alldata[91] = stripslashes($alldata[91]);
$alldata[49] = stripslashes($alldata[49]);
$alldata[51] = stripslashes($alldata[51]);
$alldata[53] = stripslashes($alldata[53]);
$alldata[68] = stripslashes($alldata[68]);
$alldata[62] = stripslashes($alldata[62]);
$alldata[44] = stripslashes($alldata[44]); //not shown in form
$alldata[46] = stripslashes($alldata[46]); //not shown in form

// adjust the indexes of the list vars:

$EMON = $alldata[37];
$WATERN = $alldata[26];
$CHANN = $alldata[27];
$DIAN = $alldata[28];
$BRAN = $alldata[29];
$JURN = $alldata[88];
$MARKN = $alldata[47];
$PLUMBN = $alldata[14];
$EQIPN = $alldata[18];
$ROPEN = $alldata[19];
$LADN = $alldata[20];
$REQN = $alldata[17];
$GUNN = $alldata[13];
$GLACN = $alldata[16];
$T_XENIN = $alldata[81];
$T_PHYLIN = $alldata[82];
$T_BION = $alldata[83];
$BIOREDN = $alldata[84];
$BIONEWN = $alldata[85];
$INFON = $alldata[22];
$ODFN = $alldata[50];
$GMOFN = $alldata[52];
$TECHN = $alldata[21];
$TOFN = $alldata[54];
$FOTOFN = $alldata[69];
$SCINFN = $alldata[24];
$MAPGIFN = $alldata[65];
$MAPFN = $alldata[64];
$MAPC_TIN = $alldata[56];
$MAPC_HPN = $alldata[57];
$MAPC_RVP1N = $alldata[58];
$MAPC_RVP2N = $alldata[59];
$MAPC_NSN = $alldata[60];
$MAPC_SRN = $alldata[61];
$KROKIGIFN = $alldata[66];
$FISH_AN = $alldata[70];
$FISH_BN = $alldata[71];
$CRN = $alldata[67];
$CHECKEDN = $alldata[98]; // GPS.CHECKED
if ($alldata[98] == '1') { $CHECKEDN = 'T'; } // fix for different values for TRUE

// ######################################################
// Common part - here the dataform is included

echo "<form action='dataform.php' method='POST' enctype='application/x-www-form-urlencoded'>������������� (���. �����):<input type='text' name='ID' size=4 maxlength=4 value=$ID >&nbsp;&nbsp;���������� ���:<input type='text' name='CODE' size=6 maxlength=6 value=$alldata[3] >&nbsp;&nbsp;&nbsp;&nbsp;<font color='red'>������� �����: <input type='text' name='DUBL_ID' size=4 maxlength=4 value=$alldata[7] ></font><br>";

if ($alldata[7] <> '') { echo "<div align='center'><font color='red'>�������� � ���������! �� �� ������ ���������� ������, ��������� ����� ���-����.</font></div><br>"; }

echo "��� �� ��������:<input type='text' name='NAME' size=30 maxlength=50 value='$alldata[5]' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��������: <input type='text' name='DUBLNAME' size=30 maxlength=50 value='$alldata[6]' ><br>��������:<input type='text' name='OPIS' size=37 maxlength=60 value='$alldata[25]' ><br>��� �� ���������� �����: $alldata[93] $alldata[94] <font color='red'>(������� - ���� �� ��������� ��������!)</font>&nbsp;&nbsp;&nbsp;&nbsp; ������: $alldata[97]<br><input type='hidden' name='EKATTE' value=$alldata[97]>��� �� ��������: $alldata[95] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��� �� ��������: $alldata[96]<br>�����. ������: <input type='text' name='X' size=9 maxlength=9 value=$alldata[40] >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�����. �������: <input type='text' name='Y' size=9 maxlength=9 value=$alldata[41] >&nbsp;&nbsp;&nbsp;���. �����:<input type='text' name='IZBOD' size=30 maxlength=50 value='$alldata[30]' ><br>";

if ( $alldata[40] <> '' && $alldata[41] <> '' ) {
	echo "<font size=-2 color=brown>(������������ �� � WGS84 � �� ������� �� $alldata[44] �� $alldata[43], �.�. $alldata[42].";

	if ( $CHECKEDN == 'T' ) {
		echo " ������������ ���������!";
	}

	if ( $alldata[46] <> '' && $alldata[46] <> '; ' ) {
		echo "<br>������� GPS: &nbsp;$alldata[46].";
	}

	echo ")</font><br>";
}
echo "������ �� �����:<input type='text' name='CESTA' size=10 maxlength=10 value='$alldata[32]' >&nbsp;&nbsp;�����. �� ���. �����:<input type='text' name='METRA' size=10 maxlength=10 value='$alldata[38]' >&nbsp;������:<input type='text' name='SWNO' size=3 maxlength=3 value='$alldata[31]' >&nbsp;&nbsp;&nbsp;������ �������� � ������:<select name='MARK' title='������ ��������?' size='1'>
<option value='$alldata[47]' >$N_Y[$MARKN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
������� ������� ��������������: <textarea rows='3' cols='100' name='ZAB' maxlength=512>$alldata[45]</textarea><br><br>����� �� ��������������:";

if ($alldata[66] == 'F' || $alldata[66] == '') { echo '&nbsp;( ���� )'; }
else { echo "<a href='../SR/$ID&#95;SR.jpg' name='SR' type='image/jpeg'><img src=../SR/$ID&#95;SR.jpg alt='����� �� ��������������' width='240' height='180' hspace='240' vspace='10' align='middle' border='1'></a><br>"; }

echo "<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>���� ����� �� �������� � ��������� �� ����������</h2></div>���� �� �������� ������� �� ������������ (������ ��������): <input type='text' name='DATE' size=8 maxlength=8 value=$alldata[1] ><br>
��� �� ������: <select name='EMO' title='��� �� ������:' size='1'>
<option value=$alldata[37] >$EMOT[$EMON]</option>
<option value=''>---(���� �����)---</option>
<option value=1>������������ ������</option>
<option value=2>�������</option>
<option value=3>��������� ������</option>
<option value=4>��������� ������</option>
<option value=5>������ ������</option>
<option value=6>������� ������</option>
<option value=7>����</option>
</select><br>
���� �������: <input type='text' name='LEN' size=6 maxlength=6 value=$alldata[8] >�&nbsp;&nbsp;&nbsp;&nbsp;�����. ����������� �� �����: <input type='text' name='TOP' size=4 maxlength=4 value=$alldata[10] >�&nbsp;&nbsp;&nbsp;&nbsp;�����. ����������� �� �����: <input type='text' name='BOT' size=4 maxlength=4 value=$alldata[9] >�<br>
������: <select name='PLUMB' title='��� �� ������?' size='1'>
<option value='$alldata[14]' >$N_Y[$PLUMBN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;��������� �������� ��������?<select name='EQIP' title='��������� ��������?' size='1'>
<option value='$alldata[18]' >$N_Y[$EQIPN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;����?<select name='ROPE' title='���������� ����?' size='1'>
<option value='$alldata[19]' >$N_Y[$ROPEN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;������?<select name='LAD' title='���������� ������?' size='1'>
<option value='$alldata[20]' >$N_Y[$LADN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
����� ��������� ����������?<select name='REQ' title='���������� ����� ����������?' size='1'>
<option value='$alldata[17]' >$N_Y[$REQN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;������� ����������� ����������?<select name='GUN' title='����������� ����������?' size='1'>
<option value='$alldata[13]' >$N_Y[$GUNN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
����: <select name='WATER' title='����:' size='1'>
<option value=$alldata[26] >$WATERT[$WATERN]</option>
<option value=''>�.�.</option>
<option value=1>����</option>
<option value=2>������</option>
<option value=3>���������� �����</option>
<option value=4>�����</option>
<option value=5>� ����� � ���� �����</option>
</select>
&nbsp;&nbsp;&nbsp;&nbsp;������ (����): <input type='text' name='SIF' size=2 maxlength=2 value=$alldata[33] ><br>
���������� ����������: <input type='text' name='OSOB' size=40 value='$alldata[92]' ><br>
<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>����������������, �������� � �������������</h2></div>
��������� �������� �� �����: <input type='text' name='ALT' size=4 maxlength=4 value=$alldata[11] >�&nbsp;&nbsp;&nbsp;&nbsp;����������� �������� ��� �.�.�.: <input type='text' name='EBALT' size=4 maxlength=4 value=$alldata[12] >�<br>
������� �����? <select name='CHAN' title='������� �����?' size='1'>
<option value=$alldata[27] >$Q_N_Y[$CHANN]</option>
<option value=''>�.�.</option>
<option value=1>��</option>
<option value=2>��</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��������� �����? <select name='DIA' title='��������� �����?' size='1'>
<option value=$alldata[28] >$Q_N_Y[$DIAN]</option>
<option value=''>�.�.</option>
<option value=1>��</option>
<option value=2>��</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;����������� �����? <select name='BRA' title='����������� �����?' size='1'>
<option value=$alldata[29] >$Q_N_Y[$BRAN]</option>
<option value=''>�.�.</option>
<option value=1>��</option>
<option value=2>��</option>
</select><br>
��������� �����: <input type='text' name='ROCK' size=25 maxlength=120 value='$alldata[73]' >&nbsp;&nbsp;&nbsp;&nbsp;�������?<select name='GLAC' title='���?' size='1'>
<option value='$alldata[16]' >$N_Y[$GLACN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;���������: <input type='text' name='SEDI' size=25 maxlength=120 value='$alldata[74]' ><br>
���������: <textarea rows='2' cols='93' name='TECT'>$alldata[75]</textarea><br>
��������� ��������������: <input type='text' name='MNT' size=80 maxlength=120 value='$alldata[76]' ><br>
����� �� ������������: <input type='text' name='RELEF' size=83 maxlength=120 value='$alldata[77]' ><br>
�������� �� ��������: <input type='text' name='DYNA' size=84 maxlength=120 value='$alldata[78]' ><br>
�����������: <input type='text' name='FORMS' size=93 maxlength=120 value='$alldata[79]' ><br>
�������������: <input type='text' name='HYDROGEO' size=91 maxlength=120 value='$alldata[80]' ><br>
<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>������������ ����������</h2></div>
���������� ������:&nbsp;&nbsp;&nbsp;�����������:<select name='T_XENI' title='�����������?' size='1'>
<option value='$alldata[81]' >$N_Y[$T_XENIN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;����������:<select name='T_PHYLI' title='����������?' size='1'>
<option value='$alldata[82]' >$N_Y[$T_PHYLIN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;������������:<select name='T_BIO' title='������������?' size='1'>
<option value='$alldata[83]' >$N_Y[$T_BION]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
�������� ����. ������:<select name='BIORED' title='�������� ����. ������?' size='1'>
<option value='$alldata[84]' >$N_Y[$BIOREDN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;����:<select name='BIONEW' title='���������� ����. ������?' size='1'>
<option value='$alldata[85]' >$N_Y[$BIONEWN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
�������� ����. ������:<input type='text' name='BIO' size=60 maxlength=120 value='$alldata[86]' ><br>
������� ������� �������������� �� ����. ������:<input type='text' name='BIO_NOTES' size=58 maxlength=120 value='$alldata[87]' ><br>
����. ������:<select name='JUR' title='��.������' size='1'>
<option value=$alldata[88] >$JURT[$JURN]</option>
<option value=''>�.�.</option>
<option value=1>�������� ������</option>
<option value=2>� ����. ����</option>
<option value=3>� ���. ����</option>
<option value=4>� ��������</option>
<option value=5>������� �����</option>
<option value=6>���. ��������</option>
<option value=7>�������������</option>
<option value=8>������������</option>
<option value=9>���� ������� ������</option>
</select>&nbsp;&nbsp;���������� ��������:<input type='text' name='NORMDOC' size=40 maxlength=120 value='$alldata[89]' ><br>
����. ��������:<textarea rows='2' cols='50' name='STOP'>$alldata[90]</textarea><br>������ ��������:<input type='text' name='OTKRIL' size=30 maxlength=50 value='$alldata[91]' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

# Check if there are bibliography records for this cave:
$biblio = sqlite_fetch_array(sqlite_unbuffered_query($db_id, "select count(D_ID) from ID2BIBLIO where D_ID=$ID;"));
if (($biblio[0] <> '') & ($biblio[0] > 0)) { echo "<b><u><a target=_blank href=biblio.php?ID=$ID>������ ��������������</a></u>&nbsp;</b>"; }
else { echo "<b><u><a target=_blank href=biblio.php?ID=$ID>�������� �� ������������</a></u>&nbsp;</b>"; }

echo "<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>���� �� ��������������</h2></div>
�������� �� ������� (��):<select name='INFO' title='��� �� ��?' size='1'>
<option value='$alldata[22]' >$N_Y[$INFON]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><input type='text' name='OD' alt='������������ �� ��������� �������� - ��' size=24 maxlength=40 value='$alldata[49]' >, ��� ����:<select name='ODF' title='�� - ��� ����?' size='1'>
<option value='$alldata[50]' >$N_Y[$ODFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>";

if ($alldata[50] == 'T') { $href=textdoc_href($ID,'OD','��'); echo "&nbsp;&nbsp;$href"; }

echo "<br>������������� �������� (���): <input type='text' name='GMO' size=20 maxlength=40 value='$alldata[51]' >, ��� ����:<select name='GMOF' title='��� - ��� ����?' size='1'>
<option value='$alldata[52]' >$N_Y[$GMOFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>";

if ($alldata[52] == 'T') { $href=textdoc_href($ID,'GMO','���'); echo "&nbsp;&nbsp;$href"; }

echo "<br>���������� �������� (��):<select name='TECH' title='��� �� ��?' size='1'>
<option value='$alldata[21]' >$N_Y[$TECHN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>&nbsp;<input type='text' name='TO' alt='������������ �� ��������� �������� - ��' size=24 maxlength=40 value='$alldata[53]' >, ��� ����:<select name='TOF' title='�� - ��� ����?' size='1'>
<option value='$alldata[54]' >$N_Y[$TOFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>";

if ($alldata[54] == 'T') { $href=textdoc_href($ID,'TO','��'); echo "&nbsp;&nbsp;$href"; }

echo "<br>������ �� �����: <input type='text' name='FOTO' size=33 maxlength=40 value='$alldata[68]' >, ��� ����:<select name='FOTOF' title='������ �� ����� - ��� ����?' size='1'>
<option value='$alldata[69]' >$N_Y[$FOTOFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>";

if ($alldata[69] == 'T') { echo "&nbsp;&nbsp;<a href='../FOTO/$ID&#95;FV.jpg'>��� ��������</a>"; }

echo "<br>�����:<select name='SCINF' title='��� �� �����?' size='1'>
<option value='$alldata[24]' >$N_Y[$SCINFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>&nbsp;&nbsp;��� �� ���������:<input type='text' name='MAP' alt='������������ �� ��������� �������� - �����' size=20 maxlength=40 value='$alldata[55]' >,&nbsp;������:<input type='text' name='FT' size=4 maxlength=4 value='$alldata[62]' >,&nbsp;&nbsp;���� �����:<input type='text' name='MAPL' size=2 maxlength=2 value=$alldata[63] >,
<br>���������:<select name='MAPGIF' title='��� �� ����� - � PNG?' size='1'>
<option value='$alldata[65]' >$N_Y[$MAPGIFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>";

echo "&nbsp;&nbsp;�������� ����:<select name='MAPF' title='��� �� ����� ��� �������� ����?' size='1'>
<option value='$alldata[64]' >$N_Y[$MAPFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>";

/* Don't need SVG display any more
if ($alldata[64] == 'T') {
	if ($alldata[63] == '1') { echo "&nbsp;<a href='../VECT/$ID&#95;MAP&#95;1.svgz' type='image/svg+xml'>��� �������</a>"; }
	else { echo "&nbsp;<a href='mapgif.php?ID=$ID&amp;FT=VECT'>��� �������</a>"; }
}
else { */
	if ($alldata[65] == 'T') {
		if ($alldata[63] == '1') { echo "&nbsp;<a href='../MAP/${ID}_MAP_1.png'>��� �������</a>"; }
		else { echo "&nbsp;<a href='mapgif.php?ID=${ID}&amp;FT=PNG'>��� �������</a>"; }
//	}
}

echo "<br>������� �������: ��:<select name='MAPC_TI' title='�������� ���������� � �������?' size='1'>
<option value='$alldata[56]' >$N_Y[$MAPC_TIN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;��:<select name='MAPC_HP' title='�����. ���� � �������?' size='1'>
<option value='$alldata[57]' >$N_Y[$MAPC_HPN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;���:<select name='MAPC_RVP1' title='��� � �������?' size='1'>
<option value='$alldata[58]' >$N_Y[$MAPC_RVP1N]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;����� ���:<select name='MAPC_RVP2' title='����� ��� � �������?' size='1'>
<option value='$alldata[59]' >$N_Y[$MAPC_RVP2N]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;��:<select name='MAPC_NS' title='����. ������� � �������?' size='1'>
<option value='$alldata[60]' >$N_Y[$MAPC_NSN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>
&nbsp;&nbsp;��:<select name='MAPC_SR' title='����� �� �������������� � �������?' size='1'>
<option value='$alldata[61]' >$N_Y[$MAPC_SRN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
�����-����:<select name='KROKIGIF' title='��� �� ����� ��� ����?' size='1'>
<option value='$alldata[66]' >$N_Y[$KROKIGIFN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��� &quot;�&quot;:<select name='FISH_A' title='��� �� ��� �?' size='1'>
<option value='$alldata[70]' >$N_Y[$FISH_AN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;��� &quot;�&quot;:<select name='FISH_B' title='��� �� ��� �?' size='1'>
<option value='$alldata[71]' >$N_Y[$FISH_BN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select><br>
��. ����������:<input type='text' name='AUTHOR' size=25 maxlength=50 value='$alldata[2]' >&nbsp;&nbsp;����:<input type='text' name='KLUB' size=20 maxlength=50 value='$alldata[35]' >&nbsp;&nbsp;<font color='red'>���. �������� �� �����������:<select name='CR' title='�������� �� ������ �� �����������?' size='1'>
<option value='$alldata[67]' >$N_Y[$CRN]</option>
<option value=''>�.�.</option>
<option value='F'>��</option>
<option value='T'>��</option>
</select></font><br>
<font color='red'><div align='center'><p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
������������� (���. �����): $ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��� �� ��������: $alldata[5]<br>";

// preserve values not used in form
echo "<input type='hidden' name='Z' value=$alldata[42]><input type='hidden' name='GPS_DATE' value='$alldata[43]'><input type='hidden' name='GPS_WHO' value='$alldata[44]'><input type='hidden' name='GPS_NOTES' value='$alldata[46]'>";

if ($alldata[7] <> '') { echo '�������� � ��������� - �� ������ �� ������� �������! ��������� ����� �� ������������ ��� �������� �����.<br>'; }  // Warn for duplicate cave

echo "��������� �� ���������? <select name='UPDATE' title='�����?' size='1'>
<option value='', selected=''>��</option>
<option value='1'>��</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;<input type='submit' name='�����' value='�����' size=5 maxlength=5 alt='�����'></div></font>
   </form>";

// End of dataform
// ######################################################

sqlite_close ($db_id);

?>

<!--here we have our php code done and proceed with the end of HTML code-->
<hr>
<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004,2009 �.</div></font>
</body>
</html>
