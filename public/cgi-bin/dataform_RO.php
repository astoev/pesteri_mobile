<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>PESTERI - ����� �� ������</title>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
</head>
<body bgcolor="#DFFFF9">
<div align="center"><h2><font color="#C00000"><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� "PESTERI"</strong></font></h2></div>
<div align="center"><hr></div>
<div align="center"><h2>����� �� ������<b></b></h2></div><br>

<?php
/* This file displays read-only info about a cave and can be called by a GET request from external app.
(c) Ivo Tachev, ivotachev@mail.bg
License: The PHP License, http://www.php.net/license/3_0.txt
*/

// Some initialization, just to be sure
$UPDATE='';
$EKATTE='';
$ID='';
include 'sqlite3.php';
$tt_database="pesteri";
$DB_version="1.2"; // required database schema version

// Define lists of option values as arrays
// When updating them do not forget to change the '<OPTION>' values in the dataform accordingly!
$EMOT = array ('�.�.', '������������ ������', '�������', '��������� ������', '��������� ������', '������ ������', '������� ������', '����');
$WATERT = array('�.�.', '����', '������', '���������� �����', '�����', '� ����� � ���� �����');
$Q_N_Y = array('�.�.', '��', '��');
$N_Y = array('F'=>'��', 'T'=>'��', '0'=>'��', '1'=>'��', ''=>'�.�.');
$JURT = array('�.�.', '�������� ������', '� ����. ����', '� ���. ����', '� ��������', '������� �����', '���. ��������', '�������������', '������������', '���� ������� ������');

// Fetch GET vars
$prm = $_GET;
$ID = $prm['ID']; // we shall need this in all cases
$EKATTE = $prm['EKATTE']; // we shall need this in all cases


if ( $ID ) { // ID is a required parameter

$db_id = sqlite_open($tt_database,0666);

// Check database version:
$sql="select * from VERSION;";
$DBver = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
if ( $DB_version <> $DBver[0] ) { echo "<div align=center><font color=red><b>������ ������ �� ����������� �� ������ �����!</b></font></div>"; }

// We are just querying below
if ($EKATTE > 0) {
	if ($ID <> '') {  // We have just changed EKATTE, else this is a new entry with EKATTE selected
		$sql = "SELECT PEST.*,GIS.D_ID,GIS.X,GIS.Y,GIS.Z,GIS.DATE,GIS.WHO,GIS.ZAB,GIS.GPS_NOTES,GIS.MARK,MAP.*,INFO.*,GIS.CHECKED FROM PEST,GIS,MAP,INFO WHERE PEST.D_ID = $ID AND GIS.D_ID = $ID AND MAP.D_ID = $ID AND INFO.D_ID = $ID;";  // Note: TVM, NASMES, OBSTINA and OBLAST are the last four values in the resultset
		$alldata = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
	}
	$sql = "SELECT EK_ATTE.T_V_M,EK_ATTE.NAME,EK_OBST.NAME,EK_OBL.NAME FROM EK_ATTE,EK_OBST,EK_OBL WHERE EK_ATTE.EKATTE = $EKATTE AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA;";  // we shall display location by the EKATTE value passed
	$nm = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
	$alldata[93] = $nm[0];
	$alldata[94] = $nm[1];
	$alldata[95] = $nm[2];
	$alldata[96] = $nm[3];
	$alldata[97] = $EKATTE;
}

else {
	$sql = "SELECT PEST.*,GIS.D_ID,GIS.X,GIS.Y,GIS.Z,GIS.DATE,GIS.WHO,GIS.ZAB,GIS.GPS_NOTES,GIS.MARK,MAP.*,INFO.*,EK_ATTE.T_V_M,EK_ATTE.NAME,EK_OBST.NAME,EK_OBL.NAME,EK_ATTE.EKATTE,GIS.CHECKED FROM PEST,GIS,MAP,INFO,EK_ATTE,EK_OBST,EK_OBL WHERE PEST.D_ID = $ID AND GIS.D_ID = $ID AND MAP.D_ID = $ID AND INFO.D_ID = $ID AND EK_ATTE.EKATTE = ( SELECT EKATTE FROM PEST WHERE D_ID = $ID ) AND EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA;";  // Note: TVM, NASMES, OBSTINA and OBLAST are the last four values in the resultset
	$alldata = sqlite_fetch_array(sqlite_unbuffered_query($db_id,$sql));
}

// unescape quotes in text values
$alldata[35] = stripslashes($alldata[35]);
$alldata[55] = stripslashes($alldata[55]);
$alldata[2] = stripslashes($alldata[2]);
$alldata[5] = stripslashes($alldata[5]);
$alldata[6] = stripslashes($alldata[6]);
$alldata[25] = stripslashes($alldata[25]);
$alldata[30] = stripslashes($alldata[30]);
$alldata[32] = stripslashes($alldata[32]);
$alldata[45] = stripslashes($alldata[45]);
$alldata[46] = stripslashes($alldata[46]); // GPS_NOTES
$alldata[92] = stripslashes($alldata[92]);
$alldata[75] = stripslashes($alldata[75]);
$alldata[76] = stripslashes($alldata[76]);
$alldata[77] = stripslashes($alldata[77]);
$alldata[78] = stripslashes($alldata[78]);
$alldata[79] = stripslashes($alldata[79]);
$alldata[80] = stripslashes($alldata[80]);
$alldata[86] = stripslashes($alldata[86]);
$alldata[87] = stripslashes($alldata[87]);
$alldata[89] = stripslashes($alldata[89]);
$alldata[90] = stripslashes($alldata[90]);
$alldata[91] = stripslashes($alldata[91]);
$alldata[49] = stripslashes($alldata[49]);
$alldata[51] = stripslashes($alldata[51]);
$alldata[53] = stripslashes($alldata[53]);
$alldata[68] = stripslashes($alldata[68]);
$alldata[62] = stripslashes($alldata[62]);
$alldata[44] = stripslashes($alldata[44]); //not shown in form
$alldata[46] = stripslashes($alldata[46]); //not shown in form

// adjust the indexes of the list vars:

$EMON = $alldata[37];
$WATERN = $alldata[26];
$CHANN = $alldata[27];
$DIAN = $alldata[28];
$BRAN = $alldata[29];
$JURN = $alldata[88];
$MARKN = $alldata[47];
$PLUMBN = $alldata[14];
$EQIPN = $alldata[18];
$ROPEN = $alldata[19];
$LADN = $alldata[20];
$REQN = $alldata[17];
$GUNN = $alldata[13];
$GLACN = $alldata[16];
$T_XENIN = $alldata[81];
$T_PHYLIN = $alldata[82];
$T_BION = $alldata[83];
$BIOREDN = $alldata[84];
$BIONEWN = $alldata[85];
$INFON = $alldata[22];
$ODFN = $alldata[50];
$GMOFN = $alldata[52];
$TECHN = $alldata[21];
$TOFN = $alldata[54];
$FOTOFN = $alldata[69];
$SCINFN = $alldata[24];
$MAPGIFN = $alldata[65];
$MAPFN = $alldata[64];
$MAPC_TIN = $alldata[56];
$MAPC_HPN = $alldata[57];
$MAPC_RVP1N = $alldata[58];
$MAPC_RVP2N = $alldata[59];
$MAPC_NSN = $alldata[60];
$MAPC_SRN = $alldata[61];
$KROKIGIFN = $alldata[66];
$FISH_AN = $alldata[70];
$FISH_BN = $alldata[71];
$CRN = $alldata[67];
$CHECKEDN = $alldata[98]; // GPS.CHECKED
if ($alldata[98] == '1') { $CHECKEDN = 'T'; } // fix for different values for TRUE

// ######################################################
// Common part - here the dataform is included

echo "������������� (���. �����):&nbsp;<font color=blue><b>$ID</b></font>&nbsp;&nbsp;&nbsp;���������� ���:&nbsp;<font color=blue><b>$alldata[3]</b></font> &nbsp;&nbsp;&nbsp;&nbsp;<font color='red'>������� �����: &nbsp;<b>$alldata[7]</b></font><br>";

if ($alldata[7] <> '') { echo "<div align='center'><font color='red'>�������� � ���������! �� �� ������ ���������� ������, ��������� ����� ���-����.</font></div><br>"; }

echo "��� �� ��������:&nbsp;<font color=blue><b>$alldata[5]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��������:&nbsp;<font color=blue><b>$alldata[6]</b></font><br>��������:&nbsp;<font color=blue><b>$alldata[25]</b></font><br>��� �� ���������� �����: <font color=blue><b>$alldata[93] $alldata[94]</b></font>&nbsp;&nbsp;&nbsp;&nbsp; ������: <font color=blue><b>$alldata[97]</b></font><br>��� �� ��������: <font color=blue><b>$alldata[95]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��� �� ��������: <font color=blue><b>$alldata[96]</b></font><br>�����. �������: &nbsp;<font color=blue><b>$alldata[40]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�����. ������: &nbsp;<font color=blue><b>$alldata[41]</b></font>&nbsp;&nbsp;&nbsp;���. �����:&nbsp;<font color=blue><b>$alldata[30]</b></font><br>";

if ( $alldata[40] <> '' && $alldata[41] <> '' ) {
	echo "<font size=-2 color=brown>(������������ �� � WGS84 � �� ������� �� $alldata[44] �� $alldata[43], �.�. $alldata[42].";

	if ( $CHECKEDN == 'T' ) {
		echo " ������������ ���������!";
	}

	if ( $alldata[46] <> '' && $alldata[46] <> '; ' ) {
		echo "<br>������� GPS: &nbsp;$alldata[46]";
	}
	echo ")</font><br>";
}
echo "������ �� �����:&nbsp;<font color=blue><b>$alldata[32]</b></font><br>�����. �� ���. �����:&nbsp;<font color=blue><b>$alldata[38]</b></font>&nbsp;������:&nbsp;<font color=blue><b>$alldata[31]</b></font>&nbsp;&nbsp;&nbsp;������ �������� � ������:&nbsp;<font color=blue><b>$N_Y[$MARKN]</b></font><br>
������� ������� ��������������: &nbsp;<font color=blue><b>$alldata[45]</b></font><br><br>����� �� ��������������:";

if ($alldata[66] == 'F' || $alldata[66] == '') { echo '&nbsp;<font color=blue>( ���� )</font>'; }
else { echo "<a href='../SR/$ID&#95;SR.jpg' name='SR' type='image/jpeg'><img src=../SR/$ID&#95;SR.jpg alt='����� �� ��������������' width='240' height='180' hspace='240' vspace='10' align='middle' border='1'></a><br>"; }

echo "<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>���� ����� �� �������� � ��������� �� ����������</h2></div>���� �� �������� ������� �� ������������ (������ ��������): &nbsp;<font color=blue><b>$alldata[1]</b></font><br>
��� �� ������: &nbsp;<font color=blue><b>$EMOT[$EMON]</b></font><br>
���� �������: &nbsp;<font color=blue><b>$alldata[8] �</b></font>&nbsp;&nbsp;&nbsp;&nbsp;�����. ����������� �� �����: &nbsp;<font color=blue><b>$alldata[10] �</b></font>&nbsp;&nbsp;&nbsp;&nbsp;�����. ����������� �� �����: &nbsp;<font color=blue><b>$alldata[9] �</b></font><br>
������: &nbsp;<font color=blue><b>$N_Y[$PLUMBN]</b></font>
&nbsp;&nbsp;&nbsp;&nbsp;��������� �������� ��������?&nbsp;<font color=blue><b>$N_Y[$EQIPN]</b></font>
&nbsp;&nbsp;����?&nbsp;<font color=blue><b>$N_Y[$ROPEN]</b></font>
&nbsp;&nbsp;������?&nbsp;<font color=blue><b>$N_Y[$LADN]</b></font><br>
����� ��������� ����������?&nbsp;<font color=blue><b>$N_Y[$REQN]</b></font>
&nbsp;&nbsp;&nbsp;&nbsp;������� ����������� ����������?&nbsp;<font color=blue><b>$N_Y[$GUNN]</b></font><br>
����: &nbsp;<font color=blue><b>$WATERT[$WATERN]</b></font>
&nbsp;&nbsp;&nbsp;&nbsp;������ (����): &nbsp;<font color=blue><b>$alldata[33]</b></font><br>
���������� ����������: &nbsp;<font color=blue><b>$alldata[92]</b></font><br>
<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>����������������, �������� � �������������</h2></div>
��������� �������� �� �����: &nbsp;<font color=blue><b>$alldata[11] �</b></font>&nbsp;&nbsp;&nbsp;&nbsp;����������� �������� ��� �.�.�.: &nbsp;<font color=blue><b>$alldata[12] �</b></font><br>
������� �����? &nbsp;<font color=blue><b>$Q_N_Y[$CHANN]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��������� �����? &nbsp;<font color=blue><b>$Q_N_Y[$DIAN]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;����������� �����? &nbsp;<font color=blue><b>$Q_N_Y[$BRAN]</b></font><br>
��������� �����: &nbsp;<font color=blue><b>$alldata[73]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;�������?&nbsp;<font color=blue><b>$N_Y[$GLACN]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;���������: &nbsp;<font color=blue><b>$alldata[74]</b></font><br>
���������: &nbsp;<font color=blue><b>$alldata[75]</b></font><br>
��������� ��������������: &nbsp;<font color=blue><b>$alldata[76]</b></font><br>
����� �� ������������: &nbsp;<font color=blue><b>$alldata[77]</b></font><br>
�������� �� ��������: &nbsp;<font color=blue><b>$alldata[78]</b></font><br>
�����������: &nbsp;<font color=blue><b>$alldata[79]</b></font><br>
�������������: &nbsp;<font color=blue><b>$alldata[80]</b></font><br>
<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>������������ ����������</h2></div>
���������� ������:&nbsp;&nbsp;&nbsp;�����������:&nbsp;<font color=blue><b>$N_Y[$T_XENIN]</b></font>
&nbsp;&nbsp;����������:&nbsp;<font color=blue><b>$N_Y[$T_PHYLIN]</b></font>
&nbsp;&nbsp;������������:&nbsp;<font color=blue><b>$N_Y[$T_BION]</b></font><br>
�������� ����. ������:&nbsp;<font color=blue><b>$N_Y[$BIOREDN]</b></font>
&nbsp;&nbsp;����:&nbsp;<font color=blue><b>$N_Y[$BIONEWN]</b></font><br>
�������� ����. ������:&nbsp;<font color=blue><b>$alldata[86]</b></font><br>
������� ������� �������������� �� ����. ������:&nbsp;<font color=blue><b>$alldata[87]</b></font><br>
����. ������:&nbsp;<font color=blue><b>$JURT[$JURN]</b></font>&nbsp;&nbsp;���������� ��������:&nbsp;<font color=blue><b>$alldata[89]</b></font><br>
����. ������� � �������:&nbsp;<font color=blue><b>$alldata[90]</b></font>&nbsp;&nbsp;&nbsp;������ ��������:&nbsp;<font color=blue><b>$alldata[91]</b></font><br><br>������������:&nbsp;";

# Check if there are bibliography records for this cave:
$biblio = sqlite_fetch_array(sqlite_unbuffered_query($db_id, "select count(D_ID) from ID2BIBLIO where D_ID=$ID;"));
// echo "Za $ID: $biblio[0]"; //debug
if (($biblio[0] <> '') & ($biblio[0] > 0)) {
	$y=0;
	$sql = "SELECT BIBLIO.*,ID2BIBLIO.* FROM ID2BIBLIO,BIBLIO WHERE ID2BIBLIO.D_ID=$ID and ID2BIBLIO.M3=BIBLIO.M3;";
	$qrez=sqlite_unbuffered_query($db_id,$sql);
	while ($bibdata = sqlite_fetch_array($qrez)) {
		$y=$y + 1;
		echo "<br><font color=blue>$y. $bibdata[4] $bibdata[5] $bibdata[2] $bibdata[3] $bibdata[9] $bibdata[6] $bibdata[10] ��.$bibdata[11] $bibdata[13] $bibdata[12]; ���.:  $bibdata[23]</font>";
	}
}
else { echo "<font color=blue><b>(����)</b></font>"; }

echo "<p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
<div align='center'><h2>���� �� ��������������</h2></div>
�������� �� ������� (��):&nbsp;<font color=blue><b>$N_Y[$INFON] &nbsp;$alldata[49]</b></font>, ��� ����:&nbsp;<font color=blue><b>$N_Y[$ODFN]</b></font>";

if ($alldata[50] == 'T') { $fnam = "../OD/${ID}_OD.txt";
	if (file_exists($fnam)) { echo "<div><object data='../OD/${ID}_OD.txt' type='text/plain' width='640'></div>"; }
	else { echo "&nbsp;&nbsp;<IMG SRC='../OD/${ID}_OD.png' type='image/png' alt='�������� �� �������' title='�������� �� �������' align='middle' border='1'>"; }
}

echo "<br>������������� �������� (���): &nbsp;<font color=blue><b>$alldata[51]</b></font>, ��� ����:&nbsp;<font color=blue><b>$N_Y[$GMOFN]</b></font>";

if ($alldata[52] == 'T') { $fnam = "../GMO/${ID}_GMO.txt";
	if (file_exists($fnam)) { echo "<div><object data='../GMO/${ID}_GMO.txt' type='text/plain' width='640'></div>"; }
	else { echo "&nbsp;&nbsp;<IMG SRC='../GMO/${ID}_GMO.png' type='image/png' alt='������������� ��������' title='������������� ��������' align='middle' border='1'>"; }
}

echo "<br>���������� �������� (��):&nbsp;<font color=blue><b>$N_Y[$TECHN] &nbsp;&nbsp;$alldata[53]</b></font>, ��� ����:&nbsp;<font color=blue><b>$N_Y[$TOFN]</b></font>";

if ($alldata[54] == 'T') { $fnam = "../TO/${ID}_TO.txt";
	if (file_exists($fnam)) { echo "<div><object data='../TO/${ID}_TO.txt' type='text/plain' width='640'></div>"; }
	else { echo "&nbsp;&nbsp;<IMG SRC='../TO/${ID}_TO.png' type='image/png' alt='���������� ��������' title='���������� ��������' align='middle' border='1'"; }
}

echo "<br>������ �� �����: &nbsp;<font color=blue><b>$alldata[68]</b></font>, ��� ����:&nbsp;<font color=blue><b>$N_Y[$FOTOFN]</b></font>";

if ($alldata[69] == 'T') { echo "&nbsp;&nbsp;<a href='../FOTO/$ID&#95;FV.jpg' name='FOTO' type='image/jpeg'><img src=../FOTO/$ID&#95;FV.jpg alt='������ �� �����' title='������ �� �����' hspace='240' vspace='10' align='middle' border='1'></a>"; }

echo "<br>�����:&nbsp;<font color=blue><b>$N_Y[$SCINFN]</b></font>&nbsp;&nbsp;��� �� ���������:&nbsp;<font color=blue><b>$alldata[55]</b></font>,&nbsp;������:&nbsp;<font color=blue><b>$alldata[62]</b></font>,&nbsp;&nbsp;���� �����:&nbsp;<font color=blue><b>$alldata[63]</b></font>,
<br>���������:&nbsp;<font color=blue><b>$N_Y[$MAPGIFN]</b></font>";

echo "&nbsp;&nbsp;�������� ����:&nbsp;<font color=blue><b>$N_Y[$MAPFN]</b></font>";

if ($alldata[65] == 'T') {
	if ($alldata[63] == '1') { echo "&nbsp;<IMG SRC='../MAP/${ID}_MAP_1.png' name='MAP' type='image/png' alt='����� �� ��������' title='����� �� ��������' align='middle' border='1' />"; }
}

echo "<br>������� �������: ��:&nbsp;<font color=blue><b>$N_Y[$MAPC_TIN]</b></font>
&nbsp;&nbsp;��:&nbsp;<font color=blue><b>$N_Y[$MAPC_HPN]</b></font>
&nbsp;&nbsp;���:&nbsp;<font color=blue><b>$N_Y[$MAPC_RVP1N]</b></font>
&nbsp;&nbsp;����� ���:&nbsp;<font color=blue><b>$N_Y[$MAPC_RVP2N]</b></font>
&nbsp;&nbsp;��:&nbsp;<font color=blue><b>$N_Y[$MAPC_NSN]</b></font>
&nbsp;&nbsp;��:&nbsp;<font color=blue><b>$N_Y[$MAPC_SRN]</b></font><br>
�����-����:&nbsp;<font color=blue><b>$N_Y[$KROKIGIFN]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��� &quot;�&quot;:&nbsp;<font color=blue><b>$N_Y[$FISH_AN]</b></font>&nbsp;&nbsp;&nbsp;&nbsp;��� &quot;�&quot;:&nbsp;<font color=blue><b>$N_Y[$FISH_BN]</b></font><br>
��. ����������:&nbsp;<font color=blue><b>$alldata[2]</b></font>&nbsp;&nbsp;����:&nbsp;<font color=blue><b>$alldata[35]</b></font>&nbsp;&nbsp;<font color='red'>���. �������� �� �����������:&nbsp;<b>$N_Y[$CRN]</b></font><br>
<font color='red'><div align='center'><p>----------------------------------------------------------------------------------------------------------------------------------------------</p>
������������� (���. �����): <b>$ID</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��� �� ��������: <b>$alldata[5]</b><br>";


// End of dataform
// ######################################################

}

sqlite_close ($db_id);

?>

<!--here we have our php code done and proceed with the end of HTML code-->
<hr>
<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4 (RO)&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004, 2009 �.</div></font>
</body>
</html>
