<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title>������ - �������</title>
</head>
<body marginwidth=0 bgcolor=#dffff9>
<div align=center><h2><font color=#C00000><u>��������� ��������� �� �����������</u><br><strong>������ ��������� �� �������� � ��������<br>���� ����� &quot;PESTERI&quot;</strong></font></h2></div>
<div align=center><hr></div>
<div align=center><h3><b><font color=blue>������� �� ������ ����� �� ����������� ������</font></b></h3></div><br>

<?php
/* (c) Ivo Tachev, ivotachev@mail.bg
License: The PHP License, http://www.php.net/license/3_0.txt
*/

// First, check how we are called:
$prm = $_POST;
include 'sqlite3.php';

$db_id = sqlite_open("pesteri",0666);

// ���������, ����� �� �������� ��� ����� �������:
// For info about the database displayed after each query
$numcaves=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST;"));
$dublcaves=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE DUBL_ID != '';"));
$realcaves=($numcaves[0] - $dublcaves[0]);
$maps=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE SCINF = 'T';"));
$nomaps=($realcaves - $maps[0]);
$nocode=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE CODE = '' OR CODE = '0';"));

// ������� ������ �������:
if ($prm['spravka'] == "1") {
	echo "<div align=center><h3><b><font color=black>���������� �� ������ - �� ����������</font></b></h3></div><br><div align='center'>";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '1';"));
	echo "������������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '2';"));
	echo "��������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '3';"));
	echo "���������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '4';"));
	echo "��������� ������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '5';"));
	echo "������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '6';"));
	echo "�������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '7';"));
	echo "����: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE EMO = '0' OR EMO = '';"));
	echo "����������: $cnt[0].</div><br><br>";
	echo "<div align='center'>";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE WATER = '1';"));
	echo "����: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE WATER = '2';"));
	echo "������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE WATER = '3';"));
	echo "���������� �����: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE WATER = '4';"));
	echo "�����: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE WATER = '5';"));
	echo "� ����� � ���� �����: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM PEST WHERE WATER = '0' OR WATER = '';"));
	echo "����������: $cnt[0].</div><br><br>";
}

if ($prm['spravka'] == "2") {
	echo "<div align=center><h3><b><font color=black>���������� �� ������ - �� ���������� ������</font></b></h3></div><br><div align='center'>";
	$cntsum=0;
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '1';"));
	echo "��� �������� ������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '2';"));
	echo "� ����. ����: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '3';"));
	echo "� ���. ����: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '4';"));
	echo "� ��������: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '5';"));
	echo "������� ������: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '6';"));
	echo "���. ���������: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '7';"));
	echo "�������������: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '8';"));
	echo "�������������: $cnt[0], ";
	$cntsum=($cntsum + $cnt[0]);
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM INFO WHERE JUR = '9';"));
	echo "� ���� ������� ������: $cnt[0].<br> ";
	$cntsum=($cntsum + $cnt[0]);
	echo "���� ��� ������� ������: $cntsum.</div><br><br>";
}

if ($prm['spravka'] == "3") {
	echo "<div align=center><h3><b><font color=black>���������� �� ������ - �� ��������� �� ��������������</font></b></h3></div><br><div align=center><b><font color=black>(������ �� ���� �� ��������� � ���������� ���)</font></b></div><br><div align='center'>";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(PEST.D_ID) FROM PEST,GIS WHERE ( GIS.X <> '' AND GIS.Y <> '') AND PEST.D_ID=GIS.D_ID;"));
	$maprez=($realcaves - $cnt[0]);
	echo "����� GPS-����������: $maprez, ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE ODF = 'T';"));
	$maprez=($realcaves - $cnt[0]);
	echo "����� �������� �� �������: $maprez, ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE GMOF = 'T';"));
	$maprez=($realcaves - $cnt[0]);
	echo "����� ������������� ��������: $maprez, ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE TOF = 'T';"));
	echo "���� ����. ��������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE MAPF = 'T';"));
	echo "���� ����� ��� �������� ������: $cnt[0], ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE MAPGIF = 'T';"));
	$maprez=($realcaves - $cnt[0]);
	echo "����� �����: $maprez, ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE FOTOF = 'T';"));
	$maprez=($realcaves - $cnt[0]);
	echo "����� ���������� �� �����: $maprez, ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id,"SELECT COUNT(D_ID) FROM MAP WHERE CR = 'T';"));
	echo "���� �������� �� ������ �� �����������: $cnt[0].<br>";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id, "select count(M3) from BIBLIO;"));
	echo "�� ������� ���� $cnt[0] ������������� ������ �� ";
	$cnt=sqlite_fetch_array(sqlite_unbuffered_query($db_id, "select count(D_ID) from (select distinct D_ID from ID2BIBLIO);"));
	echo "$cnt[0] ������.</div><br>";
}

if ($prm['spravka'] == "5") {
	// ������� �� ���������:
	echo "<div align=center><h3><b><font color=black>������ � ������� ��� 1000 �</font></b></h3></div><br><div align='center'><table rules='nowrap' border='1' align='center'><tr align='center' valign='center' bgcolor='yellow'><td width='40' align='center' valign='center'>���. �</td><td width='280' align='center' valign='center'>���</td><td width='200' align='center' valign='center'>��������</td><td width='200' align='center' valign='center'>�������</td><td width='160' align='center' valign='center'>������</td><td width='60' align='center' valign='center'>������</td><td width='40' align='center' valign='center'>����. (�)</td></tr>";

	$sql = "SELECT PEST.D_ID, PEST.NAME, PEST.DUBLNAME, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME, PEST.LEN FROM PEST, EK_ATTE, EK_OBST, EK_OBL WHERE EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA AND EK_ATTE.EKATTE = PEST.EKATTE AND LEN>1000 AND LEN<1000000 ORDER BY LEN DESC;";
	$qrez = sqlite_unbuffered_query($db_id,$sql);

	while ($q_row = sqlite_fetch_array($qrez)) {
		echo "<tr align=left valign=center><td align=right valign=center>$q_row[0]</td><td align=center valign=center>$q_row[1]</td><td align=left valign=center>$q_row[2]</td><td align=left valign=center>$q_row[3] $q_row[4]</td><td align=right valign=center>$q_row[5]</td><td align=right valign=center>$q_row[6]</td><td align=left valign=center>$q_row[7]</td></tr>";
 	}

	echo '</table></div>';
}

if ($prm['spravka'] == "4") {
	// ������� �� ���������:
	echo "<div align=center><h3><b><font color=black>������ � ����������� ��� 100 �</font></b></h3></div><br><div align='center'><table rules='nowrap' border='1' align='center'><tr align='center' valign='center' bgcolor='yellow'><td width='40' align='center' valign='center'>���. �</td><td width='280' align='center' valign='center'>���</td><td width='200' align='center' valign='center'>��������</td><td width='200' align='center' valign='center'>�������</td><td width='160' align='center' valign='center'>������</td><td width='60' align='center' valign='center'>������</td><td width='40' align='center' valign='center'>����. - (�)</td><td width='40' align='center' valign='center'>�����. + (�)</td><td width='40' align='center' valign='center'>���� �����. (�)</td></tr>";

	$sql="SELECT PEST.D_ID, PEST.NAME, PEST.DUBLNAME, EK_ATTE.T_V_M, EK_ATTE.NAME, EK_OBST.NAME, EK_OBL.NAME, PEST.BOT, PEST.TOP, PEST.BOT + PEST.TOP AS DENIV FROM PEST, EK_ATTE, EK_OBST, EK_OBL WHERE EK_OBL.OBLAST = EK_ATTE.OBLAST AND EK_OBST.OBSTINA = EK_ATTE.OBSTINA AND EK_ATTE.EKATTE = PEST.EKATTE AND DENIV>100 AND DENIV<10000 ORDER BY DENIV DESC;";
	$qrez = sqlite_unbuffered_query($db_id,$sql);

	while ($q_row = sqlite_fetch_array($qrez)) {
		echo "<tr align=left valign=center><td align=right valign=center>$q_row[0]</td><td align=center valign=center>$q_row[1]</td><td align=left valign=center>$q_row[2]</td><td align=left valign=center>$q_row[3] $q_row[4]</td><td align=right valign=center>$q_row[5]</td><td align=right valign=center>$q_row[6]</td><td align=left valign=center>$q_row[7]</td><td align=left valign=center>$q_row[8]</td><td align=left valign=center>$q_row[9]</td></tr>";
 	}
	echo '</table></div>';
}

// ���� �� �� ������� �� ������ �� ����� �������:
// Info about the database displayed after each query
echo "<br><div align=center><font size=-1 color=brown>������ PESTERI ������� ����� �� $numcaves[0] ������, �� ���:<br>���������: $dublcaves[0], ������: $realcaves, ����� �����: $nomaps, �����������: $nocode[0].</font></div>";

sqlite_close ($db_id);

?>

<br><br>
<div align="center"><hr></div>
<font size="-1" color="#C05800"><div align="right">ver. 1.2pre4&nbsp;&nbsp;&nbsp;&nbsp;&copy;&nbsp;��� �����, ����, 2004-2009 �.</div></font>
</body>
</html>
