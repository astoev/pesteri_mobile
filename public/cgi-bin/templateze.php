<?php
class templateze
{
        var $template;
        var $block;
        var $start = '~';
        var $end = '~';
        var $_QError;

function templateze($template){
 $this->template = $this->get_template($template);
}
function set($tpl_var)
    {
    if (empty($tpl_var)) {
    $this->_QError("Invalid replacement input");
   }
     $items = explode("|", $tpl_var);
     $this->template = str_replace($this->start.$items[0].$this->end,"$items[1]", $this->template);
}

function get_template($template){
     if (!file_exists($template)) {
    $this->_QError("Template does not exist<br>$template");
   }
     $file_pointer = fopen($template, "r");
     $file_read = fread($file_pointer, filesize($template));
     fclose($file_pointer);
     return $file_read;
}

function setloop($array_name){
                        global $$array_name;
                        if (empty($array_name)) {
                        $this->_QError("Invalid loop data");
                       }
                        $loop_code = '';

                        $start_pos = strpos(strtolower($this->template), '<loop name="'.$array_name.'">') + strlen('<loop name="'.$array_name.'">');
                        $end_pos = strpos(strtolower($this->template), '</loop name="'.$array_name.'">');

                        $loop_code = substr($this->template, $start_pos, $end_pos-$start_pos);

                        $start_tag = substr($this->template, strpos(strtolower($this->template), '<loop name="'.$array_name.'">'),strlen('<loop name="'.$array_name.'">'));
                        $end_tag = substr($this->template, strpos(strtolower($this->template), '</loop name="'.$array_name.'">'),strlen('</loop name="'.$array_name.'">'));

                        if($loop_code != ''){
                                $new_code = '';
                                for($i=0; $i<count($$array_name); $i++){
                                        $temp_code = $loop_code;
                                        while(list($key,) = each(${$array_name}[$i])){
                                                $temp_code = str_replace($this->start.$key.$this->end,${$array_name}[$i][$key], $temp_code);
                                        }
                                        $new_code .= $temp_code;
                                }
                                $this->template = str_replace($start_tag.$loop_code.$end_tag, $new_code, $this->template);
                        }
                }



function tagchange($tag){
         if (empty($tag)) {
                        $this->_QError("Empty tag data");
                       }
         $this->start = $tag;
         $this->end = $tag;
}
function tplblock($tpl_block){

    if (empty($tpl_block)) {
    $this->_QError("Invalid block input");
   }
     $items = explode("|", $tpl_block);
     $this->block = $this->get_template($items[1]);
     $this->template = str_replace($this->start.$items[0].$this->end,$this->block, $this->template);

}

function display(){
         $this->template = stripslashes($this->template);
         return $this->template;
}

function _QError($errmes)
{
        echo "<b>ERROR:</b><br>$errmes<br>" ;
        exit();
}

}
?>